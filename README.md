### Ansible


Deploy requires **Ansible 2.3.0**, which can be installed:

- system-wide
- into [<u>virtualenv</u>](https://virtualenv.pypa.io/en/stable/)

#### Install Ansible into python virtualenv

The chose system path is for illustrative purpose, feel free to choose yours.

```bash
$ mkdir -p ~/bin/deploy
$ virtualenv ~/bin/deploy/e
# Activate the virtualenv
$ source ~/bin/deploy/e/bin/activate
(e)$ pip install ansible==2.3
```
#### Vault

Some variables are stored in [Ansible Vault](http://docs.ansible.com/ansible/latest/playbooks_vault.html).

Create file `vault_password_file` inside ansible workingdir and put vault password there.

Set path to `vault_password_file` in `ansible.cfg`, to look something like this:

```
[defaults]
vault_password_file = ./vault_password_file
```

#### Execute BOT deploy
Use **-C** for dry run.  
Use **-e "bot_rev=X.X.X"** for deploy specific bot revision. Default revision: 1.0.0  
```bash
(e)$ ansible-playbook -i inventory/botserver deploy_bot.yml -D -e "bot_rev=1.0.0"
```