# os_users role

Role to manage OS users.

## Role configuration

**os_users_create_per_user_group** (default: true) - when creating users,  also create a group with the same username and make that the user's primary group.  
**os_users_admin_group** - name of administrators group.  
**os_users_admin_gid**  
**users_default_shell** (default: /bin/bash).  
**users_create_homedirs** (default: true) - create home directories for new users.  
**os_users_admins_nopasswd** (default: true) - allow members of admin group to use nopasswd sudo.  
**os_users** - list of users dict with multiple attributes (see below in example).  
**os_users_deleted** - list of users to be deleted.  


## Creating users

The following attributes are required for each user:

**username** - The user's username.  
**name** - The full name of the user (gecos field).  
**uid** - The numeric user id for the user. This is required for uid consistency across systems.  
**password** - If a hash is provided then that will be used, but otherwise the account will be locked.  
**groups** - a list of supplementary groups for the user.  
**ssh-key** - This should be a list of ssh keys for the user. Each ssh key  should be included directly and should have no newlines.  

In addition, the following items are optional for each user:

**shell** - The user's shell. This defaults to /bin/bash. The default is configurable using the users_default_shell variable if you want to give all users the same shell, but it is different than /bin/bash.  

Example:

    ---
    os_users:
      - username: foo
        name: Foo Barrington
        groups: ['uptimeadm']
        uid: 12345
        ssh_key:
          - "ssh-rsa AAAAA.... foo@machine"
          - "ssh-rsa AAAAB.... foo2@machine"
    os_users_deleted:
      - username: bar

## Deleting users

The `os_users_deleted` variable contains a list of users who should no longer be
in the system, and these will be removed on the next ansible run. The format
is the same as for users to add, but the only required field is `username`.
